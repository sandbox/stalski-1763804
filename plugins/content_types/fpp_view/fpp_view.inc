<?php

/**
 * @file
 * Simple view content type plugin.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
if (module_exists('views')) {
  $plugin = array(
    'title' => t('View (component)'),
    'single' => TRUE,
    'defaults' => array(
      'view' => '',
    ),
    'icon' => 'icon_view.png',
    'description' => t('Add a view as a component.'),
    'category' => t('Custom'),
    //'top level' => TRUE,
  );
}

/**
 * Output function for the 'view' content type.
 */
function fpp_components_fpp_view_content_type_render($subtype, $conf, $panel_args) {
  $data = explode('|', $conf['view']);
  $view_name = $data[0];
  $display_id = $data[1];

  $block = new stdClass();
  $block->module = 'view';
  $block->delta = $view_name;
  $block->content = views_embed_view($view_name, $display_id);
  return $block;
}

/**
 * The form to select the view and display.
 */
function fpp_components_fpp_view_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $view_options = $display_options = array();

  $views = views_get_enabled_views();
  foreach ($views as $view) {
    foreach ($view->display as $display_id => $display) {
      $enabled = (isset($display->display_options['enabled'])) ? $display->display_options['enabled'] : TRUE;
      if ($enabled) {
        $view_options[$view->name . '|' . $display_id] = $view->human_name .': ' . $display->display_title;
      }
    }
  }

  $form['view'] = array(
    '#type' => 'select',
    '#options' => $view_options,
    '#default_value' => $conf['view'],
    '#title' => t('Select view'),
    '#required' => TRUE,
  );

  // Hide the 'Override title' option for now.
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;

  return $form;
}

/**
 * Save the view selection.
 */
function fpp_components_fpp_view_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('view') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a node.
 */
function fpp_components_fpp_view_content_type_admin_title($subtype, $conf) {
  // @todo get nicer title
  return $conf['view'];
}

/**
 * Display the administrative information for a view pane.
 */
function fpp_components_fpp_view_content_type_admin_info($subtype, $conf) {
  // @todo should we return the view here or not ?
  return 'No data is display here.';
}
