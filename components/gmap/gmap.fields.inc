<?php
/*
 * @file gmap.fields.inc
 * Declares the fields for the gmap fpp bundle.
 */

/**
 * Implements hook_fpp_component_<bundle>_fields_info().
 */
function fpp_components_fpp_component_gmap_fields_info() {

  $fields = array();

  // Exported field: 'fieldable_panels_pane-map-field_map_address'
  $fields['fieldable_panels_pane-gmap-field_map_address'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_map_address',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'gmap',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'simple_gmap',
          'settings' => array(
            'iframe_height' => '400',
            'iframe_width' => '600',
            'include_link' => 0,
            'include_map' => 1,
            'include_text' => 0,
            'information_bubble' => 0,
            'link_text' => 'View larger map',
            'zoom_level' => '14',
          ),
          'type' => 'simple_gmap',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'fieldable_panels_pane',
      'field_name' => 'field_map_address',
      'label' => 'Address',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '-4',
      ),
    ),
  );

  return $fields;
}