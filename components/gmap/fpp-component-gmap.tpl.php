<?php
/**
 * @file fpp-component-gmap.tpl.php
 * Main FPP component pane template
 *
 */
?>

<div class="fpp-component-gmap clearfix">

  <?php if (!empty($field_title)): ?>
  <h2><?php print $field_title ?></h2>
  <?php endif; ?>

  <?php print $field_map_address; ?>

</div>