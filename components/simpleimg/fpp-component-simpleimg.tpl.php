<?php
/**
 * @file fpp-component-simpleimg.tpl.php
 * Main FPP component pane template
 *
 */
?>

<?php if (!empty($field_title)): ?>
<h2><?php print $field_title ?></h2>
<?php endif; ?>

<?php print $field_basic_image_image ?>