<?php
/**
 * @file fpp-component-2coltext.tpl.php
 * Main FPP component pane template
 *
 */
?>

<div class="fpp-component-2coltext clearfix">

  <?php if (!empty($field_title)): ?>
  <h2><?php print $field_title ?></h2>
  <?php endif; ?>

  <?php if (!empty($field_basic_col1text)): ?>
  <div class="left"><?php print $field_basic_col1text ?></div>
  <?php endif; ?>
  <?php if (!empty($field_basic_col2text)): ?>
  <div class="right"><?php print $field_basic_col2text ?></div>
  <?php endif; ?>
</div>