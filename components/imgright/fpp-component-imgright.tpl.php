<?php
/**
 * @file fpp-component-imgright.tpl.php
 * Main FPP component pane template
 *
 */
?>

<div class="fpp-component-imgright clearfix">

  <?php if (!empty($field_title)): ?>
  <h2><?php print $field_title ?></h2>
  <?php endif; ?>

  <?php print $field_basic_image_image ?>
  <?php print $field_basic_image_text ?>

</div>