<?php


/**
 * @file simplevideo.fields.inc
 * Declares the fields for the simple video fpp bundle.
 */

/**
 * Implements hook_fpp_component_<bundle>_fields_info().
 */
function fpp_components_fpp_component_simplevideo_fields_info() {

  $fields = array ();

  if (module_exists('emfield')) {

    $fields['fieldable_panels_pane-simplevideo-field_basic_emfield_video'] = array (
      'field_config' => array (
        'translatable' => '0',
        'entity_types' => array (),
        'settings' => array (
          'display_field' => 0,
          'display_default' => 0,
          'uri_scheme' => 'public',
        ),
        'storage' => array (
          'type' => 'field_sql_storage',
          'settings' => array (),
          'module' => 'field_sql_storage',
          'active' => '1',
          'details' => array (
            'sql' => array (
              'FIELD_LOAD_CURRENT' => array (
                'field_data_field_basic_emfield_video' => array (
                  'fid' => 'field_basic_emfield_video_fid',
                  'display' => 'field_basic_emfield_video_display',
                  'description' => 'field_basic_emfield_video_description',
                ),
              ),
              'FIELD_LOAD_REVISION' => array (
                'field_revision_field_basic_emfield_video' => array (
                  'fid' => 'field_basic_emfield_video_fid',
                  'display' => 'field_basic_emfield_video_display',
                  'description' => 'field_basic_emfield_video_description',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array (
          'fid' => array (
            'table' => 'file_managed',
            'columns' => array (
              'fid' => 'fid',
            ),
          ),
        ),
        'indexes' => array (
          'fid' => array (
            0 => 'fid',
          ),
        ),
        'field_name' => 'field_basic_emfield_video',
        'type' => 'file',
        'module' => 'file',
        'active' => '1',
        'locked' => '0',
        'cardinality' => '1',
        'deleted' => '0',
        'columns' => array (
          'fid' => array (
            'description' => 'The {file_managed}.fid being referenced in this field.',
            'type' => 'int',
            'not null' => false,
            'unsigned' => true,
          ),
          'display' => array (
            'description' => 'Flag to control whether this file should be displayed when viewing content.',
            'type' => 'int',
            'size' => 'tiny',
            'unsigned' => true,
            'not null' => true,
            'default' => 1,
          ),
          'description' => array (
            'description' => 'A description of the file.',
            'type' => 'text',
            'not null' => false,
          ),
        ),
        'bundles' => array (
          'fieldable_panels_pane' => array (
            0 => 'simplevideo',
          ),
        ),
      ),
      'field_instance' => array (
        'label' => 'Emfield Video',
        'widget' => array (
          'weight' => '-3',
          'type' => 'enfield_widget',
          'module' => 'emfield',
          'active' => 1,
          'settings' => array (
            'size' => '60',
          ),
        ),
        'settings' => array (
          'file_directory' => '',
          'file_extensions' => 'txt',
          'max_filesize' => '',
          'description_field' => 0,
          'user_register_form' => false,
        ),
        'display' => array (
          'default' => array (
            'label' => 'above',
            'type' => 'file_default',
            'settings' => array (),
            'module' => 'file',
            'weight' => 1,
          ),
        ),
        'required' => false,
        'description' => '',
        'default_value' => NULL,
        'field_name' => 'field_basic_emfield_video',
        'entity_type' => 'fieldable_panels_pane',
        'bundle' => 'simplevideo',
        'deleted' => '0',
      ),
    );

  }

  if (module_exists('video')) {

    $fields['fieldable_panels_pane-video-field_basic_video'] = array (
      'field_config' => array (
        'active' => '1',
        'cardinality' => '1',
        'deleted' => '0',
        'entity_types' => array (),
        'field_name' => 'field_basic_video',
        'storage' => array (
          'type' => 'field_sql_storage',
          'settings' => array(),
          'module' => 'field_sql_storage',
          'active' => '1',
          'details' => array (
            'sql' => array (
              'FIELD_LOAD_CURRENT' => array(
                'field_data_field_basic_video' => array(
                  'fid' => 'field_basic_video_fid',
                  'thumbnail' => 'field_video_thumbnail',
                ),
              ),
              'FIELD_LOAD_REVISION' => array (
                'field_revision_field_basic_video' => array (
                  'fid' => 'field_basic_video_fid',
                  'thumbnail' => 'field_basic_video_thumbnail',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array (
          'fid' => array (
            'table' => 'file_managed',
            'columns' => array (
              'fid' => 'fid',
            ),
          ),
        ),
        'indexes' => array (
          'fid' => array (
            0 => 'fid',
          ),
        ),
        'columns' => array (
          'fid' => array (
            'description' => 'The {file_managed}.fid being referenced in this field.',
            'type' => 'int',
            'not null' => false,
            'unsigned' => true,
          ),
          'thumbnail' => array (
            'description' => 'The {file_managed}.fid being referenced for video thumbnail.',
            'type' => 'int',
            'not null' => false,
            'unsigned' => true,
          ),
        ),
        'module' => 'video',
        'settings' => array (
          'uri_scheme' => 'public',
          'uri_scheme_converted' => 'public',
          'uri_scheme_thumbnails' => 'public',
          'autoconversion' => 0,
          'thumbnail_format' => 'png',
          'autothumbnail' => 'no',
          'default_video_thumbnail' => array (
            'fid' => 0,
            'upload_button' => 'Upload',
            'remove_button' => 'Remove',
            'upload' => '',
          ),
          'preview_video_thumb_style' => 'thumbnail',
        ),
        'translatable' => '0',
        'type' => 'video',
        'bundles' => array (
          'fieldable_panels_pane' => array (
            0 => 'simplevideo',
          ),
        ),
      ),
      'field_instance' => array (
        'bundle' => 'simplevideo',
        'deleted' => '0',
        'description' => '',
        'display' => array (
          'default' => array (
            'label' => 'hidden',
            'module' => 'video',
            'settings' => array (
              'widthxheight' => '640x360',
            ),
            'type' => 'video_formatter_player',
            'weight' => '0',
          ),
        ),
        'entity_type' => 'fieldable_panels_pane',
        'field_name' => 'field_basic_video',
        'label' => 'Image',
        'required' => 1,
        'settings' => array (
          'file_directory' => 'videos/original',
          'file_extensions' => 'mp4 ogg avi mov wmv flv ogv webm',
          'max_filesize' => '',
          'default_dimensions' => '640x360',
          'user_register_form' => false,
        ),
        'widget' => array (
          'active' => 1,
          'module' => 'video',
          'settings' => array (
            'progress_indicator' => 'throbber',
          ),
          'type' => 'video_upload',
          'weight' => '1',
        ),
      ),
    );

  }

  return $fields;

}