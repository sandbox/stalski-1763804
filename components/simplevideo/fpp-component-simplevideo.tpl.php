<?php
/**
 * @file fpp-component-singlevideo.tpl.php
 * Main FPP component pane template
 *
 */
?>

<div class="fpp-component-singlevideo clearfix">

  <?php if (!empty($field_title)): ?>
  <h2><?php print $field_title ?></h2>
  <?php endif; ?>

  <?php print $field_basic_video; ?>

</div>