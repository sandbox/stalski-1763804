<?php
/**
 * @file fpp-component-videoleft.tpl.php
 * Main FPP component pane template
 *
 */
?>

<div class="fpp-component-videoleft clearfix" <?php print $id; ?>>
  <?php if (!empty($field_title)): ?>
  <h2><?php print $field_title ?></h2>
  <?php endif; ?>

  <?php if (!empty($field_basic_video)): ?>
  <div class="video"><?php print $field_basic_video ?></div>
  <?php endif; ?>
  <?php if (!empty($field_basic_video_text)): ?>
  <div class="text"><?php print $field_basic_video_text ?></div>
  <?php endif; ?>
</div>