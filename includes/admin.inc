<?php

/**
 * @file
 * Administrative pages for Fieldable Panels Panes Components.
 */

/**
 * Menu callback: Administration page for Fieldable panels pane components.
 */
function fpp_components_administration_page() {
  $output = '';

  $info = array();
  $components = module_invoke_all('fpp_bundle_info');

  foreach ($components as $component_name => $component) {

    $item = new stdClass();
    $item->name = $component_name;
    $item->title = $component['label'];
    $item->module = $component['module'];

    $item->dependencies = array('and' => array(), 'or' => array());
    if (isset($component['dependencies'])) {
      foreach ($component['dependencies'] as $key => $modules) {

        $parts = explode("|", $modules);
        if (count($parts) == 1) {
          $item->dependencies['and'][$modules] = array(
            'name' => $modules,
            'enabled' => module_exists($modules),
          );
        }
        else {
          foreach ($parts as $module_name) {
            $item->dependencies['or'][$module_name] = array(
              'name' => $module_name,
              'enabled' => module_exists($module_name),
            );
          }
        }
      }
    }

    $info[] = $item;
  }

  $header = array(t('Component'), t('Module'), t('Dependencies'), t('Download'));
  $rows = array();
  foreach($info as $component) {
    $row = array();
    $row[] = $component->title;
    $row[] = $component->module;
    $dependencies = '';
    $modules = array();
    foreach ($component->dependencies['and'] as $required_module => $required_info) {
      $modules[$required_info['name']] = l($required_info['name'], 'http://drupal.org/project/' . $required_info['name'], array('absolute' => TRUE));
      $dependencies .= '<div style="color: ' . ($required_info['enabled'] ? 'green' : 'red') . '">' . $required_info['name'] . '</div>';
    }
    if (!empty($component->dependencies['or'])) {
      $or_modules = array();
      $dependencies .= '<div>';
      foreach ($component->dependencies['or'] as $required_module => $required_info) {
        $modules[$required_info['name']] = l($required_info['name'], 'http://drupal.org/project/' . $required_info['name'], array('absolute' => TRUE));
        $or_modules[] = '<span style="color: ' . ($required_info['enabled'] ? 'green' : 'red') . '">' . $required_info['name'] . '</span>';
      }
      $dependencies .= implode(" or ", $or_modules);
      $dependencies .= '</div>';
    }
    $row[] = $dependencies;
    $row[] = implode(" | ", $modules);
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));

  return $output;

}